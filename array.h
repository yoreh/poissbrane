#ifndef C_ARRAY
#define C_ARRAY
#include <cstdlib>
#include <cstring>

class Array {
 private:
  std::size_t dim_;
  double *arr;
 public:
  Array(std::size_t);
  ~Array();
  Array(const Array &t);
  Array accumulate();
  std::size_t size() const;
  Array &operator=(const Array &t);
  double operator[](std::size_t i) const;
  double& operator[](std::size_t i);
  Array operator+(double num);
  Array operator-(double num);
  Array operator*(double num);
  Array operator/(double num);
  Array& operator+=(double num);
  Array& operator-=(double num);
  Array& operator*=(double num);
  Array& operator/=(double num);
  Array operator+(const Array& tab);
  Array& operator+=(const Array& tab);
  Array operator-(const Array& tab);
  Array& operator-=(const Array& tab);
  Array operator*(const Array& tab);
  Array& operator*=(const Array& tab);
  friend std::ostream& operator<<(std::ostream&, Array&);
};

Array operator+(double num, const Array& tab);
Array operator*(double num, const Array& tab);
#endif
