#include <iostream>
#include "poissbrane.h"

double Poissbrane::potential(double z)
{
	double interval = (zmax_ - zmin_) / n_;
	int index = int((z - zmin_) / interval);
	
	return potential_[index];
}

Array Poissbrane::density(const std::vector<std::vector<double> >& z, const std::vector<double>& charges)
{
	double interval = (zmax_ - zmin_) / n_;
	Array dens(number_of_atoms);

	for(int frame = 0; frame < number_of_frames; ++frame)
	{
		Array d(number_of_atoms);
	
		for(int number=0; number < number_of_atoms; ++number)
		{
			int index = int((z[frame][number] - zmin_) / interval);
			d[index] += charges[number];
		}
		
		dens += d;
	}
	
	return dens;
}

Array& Poissbrane::average(Array& density)
{
	density /= number_of_frames;
	
	return density;
}

Array Poissbrane::poisson(Array& density)
{
 	double interval = fabs(zmax_ - zmin_) / (double)n_;
	density *= interval;
	Array f = density.accumulate();
	
	Array zpos(n_);
	for(int i=0;i<n_;i++)
	  zpos[i] = zmin_ + (i-1)*interval + 0.5*interval;

	density *= zpos;
	Array l = density.accumulate();
	
	Array p(number_of_atoms);
	p = zpos * f - l;
	
	double eps0 = 8.85e-12;
	double e_to_c = 1.6e-19;
	double ang_to_m = 1e-10;
	
	double u = 1/eps0 * e_to_c/(ang_to_m * ang_to_m * ang_to_m) * ang_to_m * ang_to_m;
	
	double pbc_factor = l[l.size() - 1] / fabs(zmax_ - zmin_);
	
	Array mV = (p + pbc_factor * zpos[0]) * u / (94 * 94 * interval);
	
	return mV;
}

std::vector<std::vector<double> > Poissbrane::potential_table()
{
  double interval = (zmax_ - zmin_) / n_;
  std::vector<std::vector<double> > result;
  for(int i=0;i<n_;i++) {
    std::vector<double> temp(2);
    temp[0] = (zmin_+i*interval);
    temp[1] = (potential_[i]);
    result.push_back(temp);
  }

  return result;
}

void Poissbrane::accumulate(const std::vector<std::vector<double> >& z, const std::vector<double>& charges)
{
	Array d = density(z, charges);
	d = average(d);
	d = poisson(d);
	
	potential_ = d;
}
