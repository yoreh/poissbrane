#ifndef C_POISSBRANE
#define C_POISSBRANE
#include "array.h"
#include <cmath>
#include <cstdlib>
#include <vector>

class Poissbrane {
 private:
  std::size_t n_; 
  Array potential_;
  double zmax_;
  double zmin_;
  std::size_t number_of_frames;
  std::size_t number_of_atoms;
  Array density(const std::vector<std::vector<double> >& z, const std::vector<double>& charges);
  Array poisson(Array& density);
  Array& average(Array& density);
  
 public:
  void accumulate(const std::vector<std::vector<double> >& z, const std::vector<double>& charges);
  double potential(double z);
  std::vector<std::vector<double> > potential_table();
  Poissbrane(std::size_t n, double zmax, double zmin, std::size_t nof, std::size_t noa):n_(n), zmax_(zmax), zmin_(zmin), number_of_frames(nof), number_of_atoms(noa), potential_(n_) {};
};
#endif
