import MDAnalysis as md
import numpy as np

u = md.Universe('/home/jakub/final/m2_qnb_bgc/m2_8.psf', '/home/jakub/final/praca/m2-04.long.1.dcd')
u.trajectory.skip = 1000

s = u.selectAtoms('resname POPE')
charges = s.charges()

frame = 0
l = list()
l.append(charges)

for t in u.trajectory:
	frame += 1
	coords = s.coordinates() - s.centerOfMass()
	z = coords[:, 2]
	z -= np.average(z)	
	l.append(z)

l = np.array(l)
f = open('/home/jakub/cpp/poissbrane/ch.data', 'w')

for e in l:
	for item in e:
		f.write("%s " % item)
	f.write("\n")


