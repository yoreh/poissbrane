#include <iostream>
#include "array.h"

Array::Array(std::size_t dim)
{
  dim_ = dim;
  arr = new double[dim_]();
  
}

Array::~Array()
{
  delete[] arr;
}

Array::Array(const Array &t)
{
  dim_ = t.dim_;
  arr = new double[t.dim_];
  memcpy(arr,t.arr,sizeof(double)*t.dim_);
}

Array Array::accumulate()
{
  Array result(dim_);
  result[0] = arr[0];
  for(int i=1;i<dim_;i++)
    result[i] = result[i-1] + arr[i];
  
  return result;
}

Array& Array::operator=(const Array &t)
{
  if(&t == this)
    return *this;
  
  delete[] arr;
  
  dim_ = t.dim_;
  arr = new double[t.dim_];
  memcpy(arr,t.arr,sizeof(double)*t.dim_);
  
  return *this;
}

double Array::operator[](std::size_t i) const
{
  return arr[i];
}

double& Array::operator[](std::size_t i)
{
  return arr[i];
}

Array Array::operator+(double num)
{
  Array result(dim_);
  for(int i=0;i<dim_;i++)
    result[i] = arr[i] + num;
  return result;
}

Array Array::operator-(double num)
{
  Array result(dim_);
  for(int i=0;i<dim_;i++)
    result[i] = arr[i] - num;
  return result;
}   

Array Array::operator*(double num)
{
  Array result(dim_);
  for(int i=0;i<dim_;i++)
    result[i] = arr[i] * num;
  return result;
}   

Array Array::operator/(double num)
{
  Array result(dim_);
  for(int i=0;i<dim_;i++)
    result[i] = arr[i] / num;
  return result;
}

Array &Array::operator+=(double num)
{
  for(int i=0;i<dim_;i++)
    arr[i] += num;
  return *this;
}

Array &Array::operator-=(double num)
{
  for(int i=0;i<dim_;i++)
    arr[i] -= num;
  return *this;
}

Array &Array::operator*=(double num)
{
  for(int i=0;i<dim_;i++)
    arr[i] *= num;
  return *this;
}

Array &Array::operator/=(double num)
{
  for(int i=0;i<dim_;i++)
    arr[i] /= num;
  return *this;
}

Array Array::operator+(const Array& tab)
{
  Array result(dim_);
  for(int i=0;i<dim_;i++)
    result[i] = arr[i] + tab[i];
  
  return result;
}

Array& Array::operator+=(const Array& tab)
{
  for(int i=0;i<dim_;i++)
    arr[i] += tab[i];

  return *this;
}

Array Array::operator-(const Array& tab)
{
  Array result(dim_);
  for(int i=0;i<dim_;i++)
    result[i] = arr[i] - tab[i];
  
  return result;
}

Array& Array::operator-=(const Array& tab)
{
  for(int i=0;i<dim_;i++)
    arr[i] -= tab[i];

  return *this;
}

Array Array::operator*(const Array& tab)
{
  Array result(dim_);
  for(int i=0;i<dim_;i++)
    result[i] = arr[i] * tab[i];
  
  return result;
}

Array& Array::operator*=(const Array& tab)
{
  for(int i=0;i<dim_;i++)
    arr[i] *= tab[i];

  return *this;
}

Array operator+(double num, const Array& tab)
{
  Array result(tab);
  for(int i=0;i<tab.size();i++)
    result[i] += num;
  return result;
}

Array operator*(double num, const Array& tab)
{
  Array result(tab);
  for(int i=0;i<tab.size();i++)
    result[i] *= num;
  return result;
}

std::size_t Array::size() const
{
	return dim_;
}

std::ostream& operator<<(std::ostream& s, Array& a)
{
	for(int i = 0; i < a.dim_; ++i) s << a[i] << ' ';
	s << '\n';
	
	return s;
}

int main_a(int argc, char *argv[])
{
  Array arr1(5);
  for(int i=0;i<5;i++)
    arr1[i] = i+1;

  Array arr2 = arr1.accumulate();

  for(int i=0;i<5;i++)
    std::cout << arr1[i] << " ";
  std::cout << std::endl;
  for(int i=0;i<5;i++)
    std::cout << arr2[i] << " ";
  std::cout << std::endl;

  Array arr3 = arr1 * arr2;
  
  for(int i=0;i<5;i++)
    std::cout << arr3[i] << " ";
  std::cout << std::endl;
  
  std::cout << arr3;

  return EXIT_SUCCESS;
}
