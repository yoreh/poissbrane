#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <algorithm>
#include "array.h"
#include "poissbrane.h"

std::vector<double> charges;
std::vector<std::vector<double> > z;

bool load_from_file(const char filename[])
{
  double temp;

  std::ifstream input(filename);
  if(input.rdstate())
    return false;

  std::string line;
  if(getline(input, line)) {
    std::istringstream is(line);
    while(!is.eof()) {
      is >> temp;
      charges.push_back(temp);
    }
  } else {
    return false;
  }

  while(getline(input, line)) {
    std::istringstream is(line);
    std::vector<double> vtemp;
    while(!is.eof()) {
      is >> temp;
      vtemp.push_back(temp);
    }
    z.push_back(vtemp);
  }

  return true;
}

double zmin()
{
  std::vector<double> mins;
  for(std::vector<std::vector<double> >::iterator it=z.begin();
      it!=z.end();it++)
    mins.push_back(*(min_element((*it).begin(),(*it).end())));

  return *(min_element(mins.begin(),mins.end()));
}

double zmax()
{
  std::vector<double> maxs;
  for(std::vector<std::vector<double> >::iterator it=z.begin();
      it!=z.end();it++)
    maxs.push_back(*(max_element((*it).begin(),(*it).end())));

  return *(max_element(maxs.begin(),maxs.end()));
}

int main(int argc, char *argv[])
{
  if(argc!=4) {
    std::cout << "Poissbrane -- numerical solutions of Poisson equation for lipid membrane.\n";
    std::cout << "Outputs discretized potential table.\n";
    std::cout << "Usage: " << argv[0] << " <data file> <output file> <number of integration intervals>\n";
    exit(EXIT_SUCCESS);
  }

  const char* dfile = argv[1];
  const char* ofile = argv[2];
  std::size_t noi = atoi(argv[3]);
		       
  if(!load_from_file(dfile)) {
    std::cout << "Error reading from file '" << dfile << "'.\b";
    exit(1);
  }

  int noa = charges.size();
  std::cout << "number of atoms: " << noa << std::endl;

  int nof = z.size();
  std::cout << "number of frames: " << nof << std::endl;

  double zminp = zmin();
  std::cout << "zmin: " << zminp << std::endl;

  double zmaxp = zmax();
  std::cout << "zmax: " << zmaxp << std::endl;

  Poissbrane brana(noi, zmaxp, zminp, nof, noa);
  brana.accumulate(z, charges);

  std::ofstream ofilestream(ofile);
  std::vector<std::vector<double> > results = brana.potential_table();
  for(std::vector<std::vector<double> >::iterator it=results.begin();
      it!=results.end();
      it++) {
    ofilestream << (*it)[0] << ' ' << (*it)[1] << std::endl;
  }

  return EXIT_SUCCESS;
}
