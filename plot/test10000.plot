reset
set size 0.95,0.95
set term pdfcairo enhanced mono font "Arial,14" linewidth 4 size 29.7cm,21cm
set output 'test10000.pdf'
#set xrange [a:b]
#set yrange [a:b]
set mxtics 5
set mytics 5
set grid x y mx my
set key top left
set xlabel "Położenie w przekroju membrany [j. u.]"
set ylabel "Napięcie [j. u.]"
set title "Wykres. Zależność napięcia od położenia w przekroju membrany (10 tys. przedziałów)"
plot 'test10000.dat' with lines notitle